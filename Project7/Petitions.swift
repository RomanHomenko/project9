//
//  Petitions.swift
//  Project7
//
//  Created by Роман Хоменко on 02.04.2022.
//

import Foundation

struct Petitions: Codable {
    var results: [Petition]
}
